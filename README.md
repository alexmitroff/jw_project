# JW Project
This is simple test project

## API reference

Get a list of pages
```bash
/api/pages/page/list/
``` 

Get a single page
```bash
/api/pages/page/<pk>/
``` 

Get a single content item
```bash
/api/content/audio/<pk>/
/api/content/text/<pk>/
/api/content/video/<pk>/
``` 

## Docker compose reference
```bash
sudo docker-compose up runserver 
#executes django-runserver.sh 

sudo docker-compose up autotests 
#executes django-autotests.sh

sudo docker-compose run web python manage.py createsuperuser 
```
