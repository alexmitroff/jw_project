from content.admin.audio import AudioAdmin
from content.admin.contentitem import ContentItemAdmin
from content.admin.text import TextAdmin
from content.admin.video import VideoAdmin
