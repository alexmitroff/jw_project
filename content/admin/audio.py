from django.contrib import admin
from content.models.audio import Audio


@admin.register(Audio)
class AudioAdmin(admin.ModelAdmin):
    search_fields = ('title',)
    list_display = ('title', 'bitrate')
    readonly_fields = ('view_count',)
