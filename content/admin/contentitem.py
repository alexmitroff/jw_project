from django.contrib import admin
from content.models.contentitem import ContentItem


@admin.register(ContentItem)
class ContentItemAdmin(admin.ModelAdmin):
    search_fields = ('item_title', )
    list_display = ('item_title', )

    def item_title(self, obj):
        return obj.title


class ContentInline(admin.TabularInline):
    model = ContentItem