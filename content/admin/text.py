from django.contrib import admin
from content.models.text import Text


@admin.register(Text)
class TextAdmin(admin.ModelAdmin):
    search_fields = ('title',)
    list_display = ('title',)
    readonly_fields = ('view_count',)
