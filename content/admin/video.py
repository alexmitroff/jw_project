from django.contrib import admin
from content.models.video import Video


@admin.register(Video)
class VideoAdmin(admin.ModelAdmin):
    search_fields = ('title',)
    list_display = ('title', 'url')
    readonly_fields = ('view_count',)
