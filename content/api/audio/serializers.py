from rest_framework import serializers
from content.models.audio import Audio


class AudioSerializer(serializers.ModelSerializer):
    audio_file = serializers.SerializerMethodField('audio_url', read_only=True)
    api_endpoint = serializers.ReadOnlyField()

    class Meta:
        model = Audio
        fields = (
            'id',
            'audio_file',
            'bitrate',
            'view_count',
            'api_endpoint'
        )

    def audio_url(self, obj):
        return obj.audio_file.url
