from django.urls import path

from content.api.audio.views import APIAudioView

app_name = 'audio'

urlpatterns = [
    path('api/content/audio/<int:pk>/', APIAudioView.as_view(), name='single'),
]