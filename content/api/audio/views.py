from content.api.audio.serializers import AudioSerializer
from content.core import APIDetailView
from content.models.audio import Audio


class APIAudioView(APIDetailView):
    Model = Audio
    SerializerClass = AudioSerializer
