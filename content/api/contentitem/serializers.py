from rest_framework import serializers
from content.models.contentitem import ContentItem

from content.api.text.serializers import TextSerializer
from content.api.audio.serializers import AudioSerializer
from content.api.video.serializers import VideoSerializer


class ContentItemSerializer(serializers.ModelSerializer):
    text = TextSerializer(read_only=True)
    audio = AudioSerializer(read_only=True)
    video = VideoSerializer(read_only=True)

    class Meta:
        model = ContentItem
        fields = (
            'id',
            'text',
            'audio',
            'video'
        )
