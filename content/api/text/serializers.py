from rest_framework import serializers
from content.models.text import Text


class TextSerializer(serializers.ModelSerializer):
    api_endpoint = serializers.ReadOnlyField()

    class Meta:
        model = Text
        fields = (
            'id',
            'text',
            'view_count',
            'api_endpoint'
        )