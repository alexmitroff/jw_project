from django.urls import path

from content.api.text.views import APITextView

app_name = 'text'

urlpatterns = [
    path('api/content/text/<int:pk>/', APITextView.as_view(), name='single'),
]