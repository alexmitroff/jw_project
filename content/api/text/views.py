from content.api.text.serializers import TextSerializer
from content.core import APIDetailView
from content.models.text import Text


class APITextView(APIDetailView):
    Model = Text
    SerializerClass = TextSerializer
