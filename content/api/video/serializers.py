from rest_framework import serializers
from content.models.video import Video


class VideoSerializer(serializers.ModelSerializer):
    api_endpoint = serializers.ReadOnlyField()

    class Meta:
        model = Video
        fields = (
            'id',
            'url',
            'subs',
            'view_count',
            'api_endpoint'
        )