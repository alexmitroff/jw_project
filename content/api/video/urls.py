from django.urls import path

from content.api.video.views import APIVideoView

app_name = 'video'

urlpatterns = [
    path('api/content/video/<int:pk>/', APIVideoView.as_view(), name='single'),
]