from content.api.video.serializers import VideoSerializer
from content.core import APIDetailView
from content.models.video import Video


class APIVideoView(APIDetailView):
    Model = Video
    SerializerClass = VideoSerializer
