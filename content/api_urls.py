from django.urls import path, include

app_name = 'api-content'

urlpatterns = [
    path('', include('content.api.audio.urls')),
    path('', include('content.api.text.urls')),
    path('', include('content.api.video.urls')),
]