from rest_framework.generics import get_object_or_404
from rest_framework.response import Response
from rest_framework.views import APIView

from jw_core.funcs import increment_view_count


class APIDetailView(APIView):
    Model = None
    SerializerClass = None

    def get(self, request, pk):
        content_item = get_object_or_404(self.Model, pk=pk)
        increment_view_count(content_item)
        return Response(self.SerializerClass(content_item).data, status=200)
