from django.db import models
from django.urls import reverse_lazy

from jw_core.db.models import Title, ViewCount, CreatedModified


class Audio(Title, ViewCount, CreatedModified):
    audio_file = models.FileField(upload_to="audio")
    bitrate = models.PositiveIntegerField(default=192)

    @property
    def api_endpoint(self):
        return reverse_lazy('api-content:audio:single', kwargs={'pk': self.id})

    class Meta:
        verbose_name = 'audio file'
        verbose_name_plural = 'audio files'
        db_table = 'content.audio'
