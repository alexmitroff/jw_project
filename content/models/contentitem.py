from django.db import models

from jw_core.db.models import ViewCount, CreatedModified


class ContentItem(ViewCount, CreatedModified):

    text = models.ForeignKey('content.Text', on_delete=models.CASCADE, blank=True, null=True)
    audio = models.ForeignKey('content.Audio', on_delete=models.CASCADE, blank=True, null=True)
    video = models.ForeignKey('content.Video', on_delete=models.CASCADE, blank=True, null=True)
    page = models.ForeignKey('pages.Page', on_delete=models.PROTECT, related_name='page')

    @property
    def item(self):
        if self.text:
            return self.text
        if self.audio:
            return self.audio
        if self.video:
            return self.video
        return None

    @property
    def title(self):
        item = self.item
        if not hasattr(item, 'title'):
            return 'Empty content item'
        return item.title

    @property
    def view_count(self):
        item = self.item
        if not hasattr(item, 'view_count'):
            return 0
        return item.view_count

    class Meta:
        verbose_name = 'content item'
        verbose_name_plural = 'content items'
        db_table = 'content.content_item'

    def __str__(self):
        return self.title
