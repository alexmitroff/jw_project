from django.db import models
from django.urls import reverse_lazy

from jw_core.db.models import Title, ViewCount, CreatedModified


class Text(Title, ViewCount, CreatedModified):
    text = models.TextField()

    @property
    def api_endpoint(self):
        return reverse_lazy('api-content:text:single', kwargs={'pk': self.id})

    class Meta:
        verbose_name = 'text'
        verbose_name_plural = 'texts'
        db_table = 'content.text'
