from django.db import models
from django.urls import reverse_lazy

from jw_core.db.models import Title, ViewCount, CreatedModified


class Video(Title, ViewCount, CreatedModified):
    url = models.URLField()
    subs = models.URLField()

    @property
    def api_endpoint(self):
        return reverse_lazy('api-content:video:single', kwargs={'pk': self.id})

    class Meta:
        verbose_name = 'video file'
        verbose_name_plural = 'video files'
        db_table = 'content.video'
