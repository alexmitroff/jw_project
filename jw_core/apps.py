from django.apps import AppConfig


class JWCoreConfig(AppConfig):
    name = 'jw_core'
