from django.db import models


class Title(models.Model):
    title = models.CharField(max_length=128)

    class Meta:
        abstract = True

    def __str__(self):
        return self.title


class ViewCount(models.Model):
    view_count = models.PositiveIntegerField(default=0)

    class Meta:
        abstract = True


class CreatedModified(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    modified_at = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


class Position(models.Model):
    position = models.PositiveIntegerField(default=0)

    class Meta:
        abstract = True
        ordering = ['position']
