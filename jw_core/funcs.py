def increment_view_count(obj):

    if not hasattr(obj, 'view_count'):
        # write smth to log file
        return

    obj.view_count += 1
    obj.save()
