from django.contrib import admin
from django.urls import path, include

api_urlpatterns = [
    path('', include('pages.api_urls')),
    path('', include('content.api_urls')),
]

urlpatterns = [
    path('', include(api_urlpatterns)),
    path('admin/', admin.site.urls),
]
