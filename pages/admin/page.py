from django.contrib import admin

from content.admin.contentitem import ContentInline
from pages.models.page import Page


@admin.register(Page)
class PageAdmin(admin.ModelAdmin):
    search_fields = ('title', 'slug')
    list_display = ('title', 'slug')
    readonly_fields = ('view_count',)

    inlines = (ContentInline,)
