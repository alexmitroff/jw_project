from rest_framework import serializers
from pages.models.page import Page


class PageSerializer(serializers.ModelSerializer):
    api_endpoint = serializers.ReadOnlyField()

    class Meta:
        model = Page
        fields = (
            'id',
            'title',
            'slug',
            'view_count',
            'api_endpoint'
        )
