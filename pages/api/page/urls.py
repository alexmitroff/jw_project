from django.urls import path

from pages.api.page.views import APIPageListView, APIPageView

app_name = 'page'

urlpatterns = [
    path('api/pages/page/list/', APIPageListView.as_view(), name='list'),
    path('api/pages/page/<slug:slug>/', APIPageView.as_view(), name='single'),
]