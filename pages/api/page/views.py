from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.generics import ListAPIView, get_object_or_404

from pages.api.page.pagination import PageResultPagination
from pages.api.page.serializers import PageSerializer
from pages.models.page import Page
from content.models.contentitem import ContentItem
from content.api.contentitem.serializers import ContentItemSerializer
from jw_core.funcs import increment_view_count


class APIPageListView(ListAPIView):
    queryset = Page.objects.all()
    serializer_class = PageSerializer
    pagination_class = PageResultPagination


class APIPageView(APIView):

    def get(self, request, slug):
        page = get_object_or_404(Page, slug=slug)
        increment_view_count(page)
        data = PageSerializer(page).data

        content = ContentItem.objects.filter(page__slug=slug).select_related('text', 'audio', 'video')
        data['content'] = ContentItemSerializer(content, many=True).data
        return Response(data, status=200)
