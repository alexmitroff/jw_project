from django.urls import path, include
from pages.models.page import Page

app_name = 'api-pages'

urlpatterns = [
    path('', include('pages.api.page.urls')),
]
