from django.db import models
from django.urls import reverse_lazy

from jw_core.db.models import Title, ViewCount, CreatedModified


class Page(Title, ViewCount, CreatedModified):
    slug = models.SlugField(unique=True)

    @property
    def api_endpoint(self):
        return reverse_lazy('api-pages:page:single', kwargs={'slug': self.slug})

    class Meta:
        verbose_name = 'page'
        verbose_name_plural = 'pages'
        db_table = 'pages.page'
